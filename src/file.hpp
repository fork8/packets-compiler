#pragma once

#include <boost/filesystem.hpp>

class File
{
  public:
    File(const boost::filesystem::path &file);
    virtual ~File();

    void AddIndentation();
    void RemoveIndentation();
    void WriteLine(const std::string& line);

  private:
    boost::filesystem::ofstream _stream;
    unsigned int _indentation;
};