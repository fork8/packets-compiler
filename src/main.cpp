#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <boost/program_options.hpp>

#include "compiler.hpp"

namespace po = boost::program_options;

int main(int argc, char **argv) {
    po::options_description desc;
    desc.add_options()("input-file", po::value<std::vector<std::string>>(), "input file(s)");

    po::positional_options_description positional;
    positional.add("input-file", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(positional).run(), vm);
    po::notify(vm);

    Compiler compiler;

    int status = 0;

    if(vm.count("input-file")) {
        auto& files = vm["input-file"].as<std::vector<std::string>>();
        for(auto& file: files) {
            try {
                compiler.Compile(file);
            } catch(std::exception& e) {
                auto reason = e.what();
                if(!reason) {
                    std::cout << "Failed to compile " << file << ": Unknown reason" << std::endl;
                    status = 2;
                } else {
                    std::cout << "Failed to compile " << file << ": " << reason << std::endl;
                    status = 1;
                }
            }
        }
    }

    return status;
}