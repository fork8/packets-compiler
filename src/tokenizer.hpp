#pragma once

#include <vector>
#include <string>

//#include <boost/regex.hpp>
#include <regex>

enum TokenType {
    TOKEN_KEYWORD,
    TOKEN_IDENTIFIER,
    TOKEN_STRING,
    TOKEN_NUMBER,
    TOKEN_OPENING_BRACKET,
    TOKEN_CLOSING_BRACKET,
    TOKEN_OPENING_CURLY_BRACKET,
    TOKEN_CLOSING_CURLY_BRACKET,
    TOKEN_OPENING_SQUARE_BRACKET,
    TOKEN_CLOSING_SQUARE_BRACKET,
    TOKEN_DIRECTION_IN,
    TOKEN_DIRECTION_OUT,
    TOKEN_DIRECTION_IN_OUT,
    TOKEN_END_COMMAND,
    TOKEN_COMMENT,
    TOKEN_EOF
};

std::string TokenToString(TokenType type);

struct TokenInfo {
    std::regex regex;
    TokenType type;
    int lengthAdjust;
};

struct Token {
    TokenType type;
    std::string value;

    int lineNo;
    int pos;

    std::string ToString() {
        return TokenToString(type) + " " + value + "(" + std::to_string(lineNo) + ":" + std::to_string(pos) + ")";
    }
};

class Tokenizer
{
  public:
    Tokenizer();

    std::vector<Token> Tokenize(const std::string& file);

private:
    void AddToken(const std::string& regex, TokenType type, int lengthAdjust = 0);

private:
    std::vector<TokenInfo> _tokenInfo;
};